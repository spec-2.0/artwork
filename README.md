# spec-2.0 artwork
![spec-2.0 logo][logo]

# Licensing
![CC-BY-SA-4.0 logo][license]
* This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
* The Debian Open Use Logo(s) are Copyright (c) 1999 [Software in the Public Interest, Inc.](http://www.spi-inc.org/), and are released under the terms of the [GNU Lesser General Public License](http://www.gnu.org/copyleft/lgpl.html), version 3 or any later version, or, at your option, of the [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/).
* The RPM logo is registered trademark and copyright of [Red Hat, Inc.](https://redhat.com/), and is released under Public Domain terms.

[license]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[logo]: https://gitlab.com/spec-2.0/artwork/raw/master/robot-spec.png
